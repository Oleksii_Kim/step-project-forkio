const gulp = require("gulp");
const htmlmin = require("gulp-htmlmin");
const concat = require("gulp-concat");
const autoprefixer = require("gulp-autoprefixer");
const cssmin = require("gulp-cssmin");
const minifyjs = require('gulp-js-minify');
const rename = require("gulp-rename");
const minify = require("gulp-minify");
const deleteFiles = require("delete");
const sass = require("gulp-sass")(require("sass"));
const image = require('gulp-image');

gulp.task("html-minify", function () {
    return gulp
        .src("index.html")
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest("dist"));
});

gulp.task("buildJs", function () {
    return gulp.src("src/js/*.js")
        .pipe(minifyjs())
        .pipe(rename({suffix: ".min"}))
        .pipe(gulp.dest("dist/js"));
});
gulp.task("buildImg", function () {
    return gulp
        .src("src/img/**/*")
        .pipe(image())
        .pipe(gulp.dest('dist/img'));
});

gulp.task("deleteFiles", function () {
    return deleteFiles("dist/");
});

gulp.task("build-css", function () {
    return gulp
        .src("src/scss/*.scss")
        .pipe(
            autoprefixer({
                cascade: false,
            })
        )
        .pipe(sass().on('error', sass.logError))
        .pipe(concat("styles.css"))
        .pipe(cssmin())
        .pipe(rename({suffix: ".min"}))
        .pipe(gulp.dest("dist"));
});
gulp.task(
    "build",
    gulp.series(
        "deleteFiles",
        gulp.parallel("html-minify", "buildImg", "build-css","buildJs")
    )
);
gulp.watch(
    ["src/scss/**/**/*.scss", "*.html", "src/js/*.js"],
    gulp.series("build")
);